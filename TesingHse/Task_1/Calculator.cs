using System;
using System.Text.RegularExpressions;

namespace Task_1
{
    public class Calculator
    {
        public static double[] Calculate(double a, double b, double c)
        {
            if (a == 0 && b == 0 && c == 0)
                return new double[0];
            var d = Math.Pow(b, 2) - (4 * a * c);
            return d < 0
                ? new double[0]
                : d == 0
                    ? new[]
                    {
                        (-1 * b + Math.Sqrt(d)) / (2 * a)
                    }
                    : new[]
                    {
                        (-1 * b + Math.Sqrt(d)) / (2 * a),
                        (-1 * b - Math.Sqrt(d)) / (2 * a)
                    };
        }
    }
}