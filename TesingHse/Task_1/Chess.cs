using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace Task_1
{
    public class Chess
    {
        public static Random Random = new Random();
        private const byte MAP_SIZE = 8;

        private List<List<int>> map = Enumerable.Range(0, 8)
            .Select(item => Enumerable.Range(0, 8)
                .Select(input => 0).ToList())
            .ToList();


        private List<Figure> figures = new List<Figure>();
        public List<Figure> Figures => figures;
        private int count = 0;
        public int Count => count;

        public Chess()
        {
        }

        public void GenerateFigures(int count)
        {
            this.figures = Enumerable.Range(0, count).Select(x => new Figure()).ToList();
            this.figures.ForEach(item => this.SetFigure(item));
        }

        public bool Check()
        {
            bool cross = false;
            var figures = this.figures;

            for (int i = 0; i < figures.Count - 1; i++)
            for (int j = i + 1; j < figures.Count; j++)
                if ((Math.Abs(figures[i].Point.X - figures[j].Point.X) ==
                     Math.Abs(figures[i].Point.Y - figures[j].Point.Y)) ||
                    (figures[i].Point.X == figures[j].Point.X) ||
                    (figures[i].Point.Y == figures[j].Point.Y))
                    cross = true;
            return cross;
        }

        public void SetFigure(Figure figure)
        {
            this.figures.Add(figure);
            this.map[figure.Point.X][figure.Point.Y] = 1;
        }


        public void PrintMap()
        {
            map.ForEach((item) =>
                {
                    item.ForEach(inItem => Console.Write(inItem == 1 ? "X " : ". "));
                    Console.WriteLine();
                }
            );
        }

        public
            class Figure : IComparer
        {
            private Point point;
            public Point Point => point;

            public Figure()
            {
                this.point = new Point(Random.Next(0, 8), Random.Next(0, 8));
            }

            public Figure(Point point)
            {
                this.point = point;
            }

            public int Compare(object x, object y)
            {
                var f = (Figure) x;
                var s = (Figure) y;
                if (f.point.X == s.point.X)
                    if (f.Point.Y == s.point.Y)
                        return 0;
                    else if (f.Point.Y == s.point.Y)
                        return 0;
                return 1;
            }
        }


        public struct Point
        {
            private int x;
            private int y;

            public int X
            {
                get => x;
                set
                {
                    if (x < 8 || x >= 0) this.x = value;
                    else
                        throw new Exception("Не возможная позиция ");
                }
            }

            public int Y
            {
                get => y;
                set
                {
                    if (y < 8 || y >= 0) this.y = value;
                    else
                        throw new Exception("Не возможная позиция ");
                }
            }

            public Point(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }
    }
}