using System;
using System.Linq;
using Microsoft.VisualBasic;

namespace Task_1
{
    public static class ListRemover
    {
        /// <summary>
        /// Удаление каждого второго элемента из строки
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string RemoveFromString(this string data) => string
            .Join(',', data
                .Split(',')
                .ToList()
                .Where((x, index) => index % 2 == 0));
    }
}