using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using Task_1;

namespace Task_1_Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        [TestCase(1, 2, 3, new double[0], TestName = "Упорядоченный набор данных")]
        [TestCase(null, null, null, new double[0], TestName = "Упорядоченный набор данных")]
        [TestCase(-1, 4, -3, new double[2] {3.0d, 1.0d}, TestName = "Отрицательные числа")]
        [TestCase(0, 0, 0, new double[0], TestName = "Нулевые числа")]
        [TestCase(4, -4, 1, new double[1] {0.5},TestName = "Проверка с одним корнем")]
        [TestCase(10, 20, 30, new double[0], TestName = "Проверка больших положительных")]
        [TestCase(2, 5, 3, new double[2]
        {
            -1, -1.5
        }, TestName = "Проверка для 2-х элементов")]
        [TestCase(1.245, 2.254, 3.5434, new double[0])]
        [TestCase(1000, 2000, 400, new double[2]
        {
            -0.22540333075851662, -1.7745966692414834
        }, TestName = "Проверка большого набора данных")]
        public void TestRoots(double a, double b, double c, double[] resuls)
        {
            var results = Calculator.Calculate(a, b, c);


            Assert.IsTrue(resuls.Sum() == results.Sum() && resuls.Length == results.Length);
        }
    }
}