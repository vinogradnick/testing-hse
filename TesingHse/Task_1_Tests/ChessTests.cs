using NUnit.Framework;
using Task_1;

namespace Task_1_Tests
{
    [TestFixture]
    public class ChessTests
    {
        [Test]
        [TestCase(7, 7, 1, 3, false, TestName = "Проверка в разных концах карты")]
        [TestCase(4, 2, 1, 3, false, TestName = "Проверка на разных линиях")]
        [TestCase(1, 2, 2, 1, true, TestName = "Проверка на диагонали")]
        [TestCase(0, 0, 7, 7, true, TestName = "Проверка на диагонали одного элемента со старта, а другого с конца")]
        [TestCase(0, 0, 0, 0, true, TestName = "Проверка в одной точке")]
        [TestCase(3, 1, 0, 7, false, TestName = "Проверка ")]
        [TestCase(3, 4, 1, 3, false, TestName = "Проверка ")]
        [TestCase(4, 4, 0, 0, true, TestName = "Проверка ")]
        [TestCase(4, 1, 4, 1, true, TestName = "Проверка")]
        public void TestFigures(int pos1x, int pos1y, int pos2x, int pos2y, bool res)
        {
            var chess = new Chess();
            var f = new Chess.Figure(new Chess.Point(pos1x, pos1y));
            var s = new Chess.Figure(new Chess.Point(pos2x, pos2y));
            chess.SetFigure(f);
            chess.SetFigure(s);
            chess.PrintMap();
            Assert.AreEqual(res, chess.Check());
        }
    }
}