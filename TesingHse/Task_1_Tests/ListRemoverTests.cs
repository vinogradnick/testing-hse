﻿using System;
using System.Linq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Task_1;

namespace Task_1_Tests
{
    [TestFixture]
    class ListRemoverTests
    {
        [Test]
        [TestCase("lorem,ipsum,someday,intellegent", "lorem,someday", TestName = "Набор слов ")]
        [TestCase("a,b,c,d", "a,c",TestName = "Упорядоченный набор символов ")]
        [TestCase("a", "a",TestName = "Набор из одного элемента ")]
        [TestCase("", "",TestName = "Пустой набор ")]
        [TestCase("a,b", "a",TestName = "Набор из двух элементов ")]
        [TestCase("a,b,c,d,e,f,g,h,i,g,k,l,m", "a,c,e,g,i,k,m",TestName = "Длинный набор данных ")]
        [TestCase("1.245,65432.43,674.36,67754.321,876.53", "1.245,674.36,876.53",TestName = "Набор данных из дробных чисел ")]
        public void TestRemoveItem(string randomString, string predicated)
        {
            var start = randomString;
            var values = randomString.RemoveFromString();
            // Assert.Warn($"{start} ||  ${values}");
            Assert.AreEqual(predicated, values, " remove from string are not equal");
        }
    }
}